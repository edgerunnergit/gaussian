use std::sync::{Arc, Mutex};
use std::time::{SystemTime, UNIX_EPOCH, Duration};
use rand::Rng;
use tokio::sync::mpsc;
use rand_distr::{Distribution, Normal};
use plotters::prelude::*;

mod syscalls;

#[derive(Debug, PartialEq)]
struct Event<T: Default> {
    name: T,
    timestamp: Duration,
    delay: Duration,
}

impl<T: Default> Event<T> {
    fn new(name: T, delay: Duration) -> Self {
        let timestamp = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .expect("Time went backwards");

        Event { name, timestamp, delay }
    }
}

#[derive(Debug, Default)]
struct EventList<T: Default> {
    events: Vec<Event<T>>,
}

impl<T: Default> EventList<T> {
    fn new() -> Self {
        Default::default()
    }

    fn add_event(&mut self, name: T, delay: Duration) {
        let event = Event::new(name, delay);
        self.events.push(event);
    }

    // Calculate the mean delay of events in the list
    fn mean_delay(&self) -> Duration {
        let total_delay: Duration = self.events.iter().map(|event| event.delay).sum();
        if self.events.is_empty() {
            Duration::default()
        } else {
            total_delay / self.events.len() as u32
        }
    }

    fn median_delay(&self) -> Duration {
        let mut sorted_delays = self.events.iter().map(|event| event.delay).collect::<Vec<_>>();
        sorted_delays.sort();

        let len = sorted_delays.len();
        if len == 0 {
            Duration::default()
        } else if len % 2 == 0 {
            (sorted_delays[len / 2 - 1] + sorted_delays[len / 2]) / 2
        } else {
            sorted_delays[len / 2]
        }
    }

    // Calculate the standard deviation of delays in the list
    fn std_deviation_delay(&self) -> Duration {
        let mean = self.mean_delay().as_micros() as f64;
        let squared_diff_sum = self.events.iter()
            .map(|event| (event.delay.as_micros() as f64 - mean).powi(2))
            .sum::<f64>();

        if self.events.is_empty() {
            Duration::default()
        } else {
            Duration::from_micros((squared_diff_sum / self.events.len() as f64).sqrt() as u64)
        }
    }

    // Detect bimodality in the event list's delay distribution
    fn is_bimodal(&self) -> bool {
        let mut delays = self.events.iter().map(|event| event.delay).collect::<Vec<_>>();
        delays.sort();

        if delays.is_empty() {
            return false;
        }

        let num_delays = delays.len();
        let mid = num_delays / 2;
        let first_half = &delays[..mid];
        let second_half = &delays[mid..];

        let first_half_median = first_half[mid / 2];
        let second_half_median = second_half[mid / 2];

        let threshold = first_half_median + (second_half_median - first_half_median) / 2;

        // Count how many delays are above the threshold
        let above_threshold = delays.iter().filter(|&&delay| delay > threshold).count();

        // If more than half of the delays are above the threshold, it's likely bimodal
        above_threshold > num_delays / 2
    }
}

fn kernel_density_estimation(data: &[Duration], bandwidth: f64, x_values: &[f64]) -> Vec<f64> {
    let mut kde_values = Vec::with_capacity(x_values.len());

    for x in x_values {
        let mut kde_sum = 0.0;
        for &data_point in data {
            let x_val = data_point.as_secs_f64();
            let kernel_value = gaussian_kernel(*x, x_val, bandwidth);
            kde_sum += kernel_value;
        }
        let kde_value = kde_sum / (data.len() as f64 * bandwidth);
        kde_values.push(kde_value);
    }

    kde_values
}

fn gaussian_kernel(x: f64, xi: f64, bandwidth: f64) -> f64 {
    let diff = x - xi;
    let exponent = -0.5 * (diff / bandwidth).powi(2);
    let normalization = 1.0 / (bandwidth * (2.0 * std::f64::consts::PI).sqrt());
    normalization * exponent.exp()
}

#[tokio::main]
async fn main() {
    let event_list = Arc::new(Mutex::new(EventList::new()));
    let (sender, mut receiver) = mpsc::channel::<Event<String>>(32);
    let event_list_clone = event_list.clone(); // Clone the Arc for the spawned task

    // Spawn a task to listen to the receiver channel and add events to the event list
    tokio::spawn(async move {
        while let Some(event) = receiver.recv().await {
            let delay = event.delay;
            // tokio::time::sleep(delay).await;
            event_list_clone.lock().unwrap().add_event(event.name, delay);
        }
    });

    // Simulate sending a Gaussian distribution of syscall events with random delays
    // let num_events = 10_000;
    // let mean = 500; // Mean of the Gaussian distribution (in milliseconds)
    // let std_dev = 100; // Standard deviation of the Gaussian distribution (in milliseconds)
    // let normal = Normal::new(mean as f64, std_dev as f64).unwrap();

    // for i in 1..=num_events {
    //     let event_name = format!("Syscall Event {}", i);
    //     // let event_name = Syscalls::random_syscall();
    //     let delay_millis = normal.sample(&mut rand::thread_rng()) as i64;
    //     let delay = Duration::from_millis(delay_millis.abs() as u64);

    //     let event = Event::new(event_name.clone(), delay);
    //     sender.send(event).await.expect("Send error");
    //     println!("Sent event: {:?}", i);
    // }

    // Simulate sending a bimodal distribution of syscall events with random delays
    let num_events = 10_000;
    let mean1 = 500; // Mean of the first Gaussian distribution (in milliseconds)
    let std_dev1 = 50; // Standard deviation of the first Gaussian distribution (in milliseconds)
    let mean2 = 800; // Mean of the second Gaussian distribution (in milliseconds)
    let std_dev2 = 150; // Standard deviation of the second Gaussian distribution (in milliseconds)

    let normal1 = Normal::new(mean1 as f64, std_dev1 as f64).unwrap();
    let normal2 = Normal::new(mean2 as f64, std_dev2 as f64).unwrap();

    for i in 1..=num_events {
        let event_name = format!("Syscall Event {}", i);

        let mut rng = rand::thread_rng();
        let choose_distribution: f64 = rng.gen(); // Random number between 0 and 1

        let (delay_mean, delay_std_dev) = if choose_distribution < 0.5 {
            (mean1, std_dev1)
        } else {
            (mean2, std_dev2)
        };

        let delay_millis = if choose_distribution < 0.5 {
            normal1.sample(&mut rng) as i64
        } else {
            normal2.sample(&mut rng) as i64
        };

        let delay = Duration::from_millis(delay_millis.abs() as u64);

        let event = Event::new(event_name.clone(), delay);
        sender.send(event).await.expect("Send error");
        println!("Sent event: {:?}", i);
    }

    // Delay to allow async tasks to complete
    // tokio::time::sleep(Duration::from_secs(2)).await;

    let locked_event_list = event_list.lock().unwrap();
    // Print the eventlist
    println!("{:#?}", locked_event_list);
    // Print the mean and standard deviation
    println!("Mean delay: {:?}", locked_event_list.mean_delay());
    println!("Median delay: {:?}", locked_event_list.median_delay());
    println!("Standard deviation of delay: {:?}", locked_event_list.std_deviation_delay());

    if locked_event_list.is_bimodal() {
        println!("EventList appears to be bimodal.");
    } else {
        println!("EventList does not appear to be bimodal.");
    }

    let delay_data: &[Duration] = &locked_event_list.events.iter().map(|event| event.delay).collect::<Vec<_>>()[..];

    let bandwidth = 10.0; // Adjust bandwidth as needed
    let num_points = 100;
    let x_values: Vec<f64> = (0..num_points).map(|i| (i as f64) * (100.0 / (num_points - 1) as f64)).collect();

    let kde_values = kernel_density_estimation(&delay_data, bandwidth, &x_values);

    // Set up plotter backend
    let root = BitMapBackend::new("kde_plot.png", (800, 600)).into_drawing_area();
    root.fill(&WHITE).unwrap();

    // Create chart context
    let mut chart = ChartBuilder::on(&root)
        .margin(10)
        .x_label_area_size(40)
        .y_label_area_size(40)
        .build_cartesian_2d(0.0..100.0, 0.0..0.1).unwrap();

    // Draw KDE line series
    chart.configure_mesh().draw().unwrap();
    chart.draw_series(LineSeries::new(
        x_values.iter().zip(kde_values.iter()).map(|(x, y)| (*x, *y)),
        &RED,
    )).unwrap();

}